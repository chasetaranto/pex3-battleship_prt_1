/*
Eliezer Pla && Chaseon Taranto
*/
package battleship;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

public class Window {
    public Window(){
        JFrame window = new JFrame();
        window.setTitle("Space Battleship");
        window.setSize(1408, 704);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setMaximizedBounds(new Rectangle(1408,704));
        window.setVisible(true);
        
        PlayerAttackPanel attack = new PlayerAttackPanel();
//        attack.toggleEnableButtons(false);
        PlayerShipPlacePanel place = new PlayerShipPlacePanel(attack);
        SplashScreen splashScreen = new SplashScreen();
        
        //splash screen
        window.add(splashScreen, BorderLayout.CENTER);
        window.getContentPane().validate();
        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {}
        window.remove(splashScreen);
        
        //Game
        window.setLayout(new GridLayout(1,2));
        window.add(place);
        window.add(attack);
        window.getContentPane().validate();
        window.getContentPane().repaint();
        startBackgroundMusic();
        
    }
    private void startBackgroundMusic(){
        AudioInputStream audioIn;
        try {
            audioIn = AudioSystem.getAudioInputStream(BackgroundMusic.class.getResource("battle_ship_song.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {}
    }
}

class BackgroundMusic implements Runnable{

    @Override
    public void run() {
//        System.out.println(System.getProperty("user.dir")+"\\battle_ship_song.wav");
//        Media song = new Media((new File(System.getProperty("user.dir")+"\\battle_ship_song.wav")));
//        MediaPlayer mediaPlayer = new MediaPlayer(song);
//        mediaPlayer.play();
        AudioInputStream audioIn;
        try {
            audioIn = AudioSystem.getAudioInputStream(BackgroundMusic.class.getResource("battle_ship_song.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            while(true){
//                System.out.println(clip.isRunning());
                if(!clip.isRunning())
                    clip.start();
            }
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {}
    }
    
}