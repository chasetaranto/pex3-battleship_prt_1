/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class SplashScreen extends JPanel{
    private BufferedImage image;
    
    public SplashScreen(){
        setBackground(Color.darkGray);
        try{
            image = ImageIO.read(new File((getClass().getResource("CleanupCrewLogo.png")).toURI()));
        }catch(URISyntaxException | IOException ex){}
    }
    
    @Override
    protected void paintComponent(Graphics g){
        System.out.println(image.getHeight());
        System.out.println(image.getWidth());
        super.paintComponent(g);
        g.drawImage(image,(704/2)-(558/2),-75,this);
    }
    
}
