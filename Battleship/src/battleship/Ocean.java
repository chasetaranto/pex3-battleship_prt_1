/*
Eliezer Pla && Chaseon Taranto
*/
package battleship;

import battleship.Ships.*;
import java.security.SecureRandom;

public class Ocean {
    
    protected SecureRandom random;
    protected Ship[] armada;
    protected Ship[][] ships;     //The ocean
    protected int shotsFired;     //Total num of shots the player fired
    protected int hitCount;       //Total num of shots that hit a ship
    protected int shipsSunk;      //Num of ships sunk

    public Ocean() {
        
        random = new SecureRandom();
        ships = new Ship[10][10];
        armada = new Ship[10];
        
        Battleship battleship = new Battleship();
        Cruiser cruiser1 = new Cruiser();
        Cruiser cruiser2 = new Cruiser();
        Destroyer destroyer1 = new Destroyer();
        Destroyer destroyer2 = new Destroyer();
        Destroyer destroyer3 = new Destroyer();
        Submarine submarine1 = new Submarine();
        Submarine submarine2 = new Submarine();
        Submarine submarine3 = new Submarine();
        Submarine submarine4 = new Submarine();
        
        armada[0] = battleship;
        armada[1] = cruiser1;
        armada[2] = cruiser2;
        armada[3] = destroyer1;
        armada[4] = destroyer2;
        armada[5] = destroyer3;
        armada[6] = submarine1;
        armada[7] = submarine2;
        armada[8] = submarine3;
        armada[9] = submarine4;
        
        shotsFired = 0;
        hitCount = 0;
        shipsSunk = 0;
        for (int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                ships[i][j] = new EmptySea();
            }
        }
    }

    public void placeAllShipsRandomly() {
        int row = 0;
        int column = 0;
        boolean hori = true;
        for (int i = 0; i < 10; i++) {
            do {
                row = random.nextInt(10);
                column = random.nextInt(10);
                hori = random.nextBoolean();
            } while (!armada[i].okToPlaceShipAt(row, column, hori, this));
            armada[i].placeShipAt(row, column, hori, this);
            //System.out.println("Placed "+i+" ships.");
        }
    }

    public boolean isOccupied(int row, int column) {
        boolean occupied;
        EmptySea check = new EmptySea();
        try {
            //occupied = !ships[row][column].equals(check);
            if(ships[row][column].getClass() == check.getClass()){
                occupied = false;
            }
            else{
                occupied = true;
            }
            
        } catch(IndexOutOfBoundsException e){
            occupied = false;
        }
        return occupied;
    }

    public boolean shootAt(int row, int column) {
        boolean hit;
        if(ships[row][column].getClass() == new EmptySea().getClass()){
            hit = false;
        }
        else{
            hit = true;
        }
        shotsFired++;
        //System.out.println(shotsFired);
        if (hit) {
            hitCount++;
            //System.out.println(hitCount);
        }
        return hit;
    }

    public int getShotsFired() {
        return shotsFired;
    }

    public int getHitCount() {
        return hitCount;
    }

    public int getShipsSunk() {
        return shipsSunk;
    }

    public boolean isGameOver() {
        if (hitCount == 20) {
            return true;
        } else {
            return false;
        }
    }

    public Ship[][] getShipArray() {
        return ships;
    }
    
    public void setShipArray(int row, int column, Ship value) {
        ships[row][column] = value;
    }

    public void print() {
        for(int i = 0; i < 10;i++){
            for(int j = 0; j < 10; j++){
                System.out.print(ships[i][j].toString());
            }
            System.out.println();
        }
    }
}