package battleship;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;

public class PlayerAttackPanel extends JPanel{
    public PlayerAttackPanel(){
        super();
        boolean cheat = false;//Cheat mode control
        Ocean ocean = new Ocean();
        ocean.placeAllShipsRandomly();
        setLayout(new GridLayout(11,11));
        for(int i = 0; i < 11; i++){
            for(int j = 0; j< 11; j++){
                if(i==10 && j<10){
                    add(new JLabel("        "+j));
                }
                else if(j==10 && i<10){
                    add(new JLabel("        "+i));
                }
                else if(i <10 && j< 10)
                    add(new AttackButton(i, j, ocean, cheat));
            }
        }
    }
//    public void toggleEnableButtons(boolean state){
//        Component[] content = getComponents();
//        for (Component content1 : content) {
//            if(content1 instanceof JButton){
//                content1 = (AttackButton)content1;
//                if(state){
//                    content1.enableButton();
//                }
//                else{
//                    content1.disableButton();
//                }
//                content1.setEnabled(state);
//            }
//        }
//    }
}
