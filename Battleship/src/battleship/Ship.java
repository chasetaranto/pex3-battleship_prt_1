/*
Eliezer Pla && Chaseon Taranto
*/
package battleship;


public abstract class Ship implements ShipAction{
    private int bowRow, bowColumn, length;
    private boolean horizontal;
    private int hitCount;

    @Override
    public abstract String getShipType();

    @Override
    public boolean okToPlaceShipAt(int row, int column, boolean horizontal, Ocean ocean) {
//        System.out.println(row+" , "+column+" , "+horizontal);
        if(horizontal){
            if(column+length >9){
                return false;
            }
            if(ocean.isOccupied(row, column-1)){
                return false;
            }
            for(int i = 0; i < length; i++){
                if(ocean.isOccupied(row-1, column+i) || ocean.isOccupied(row, column+i) || ocean.isOccupied(row+1, column+i)){
                    return false;
                }
            }
            if(ocean.isOccupied(row, column+length)){
                return false;
            }
        }
        else{
            if(row+length >9){
                return false;
            }
            if(ocean.isOccupied(row-1, column)){
                return false;
            }
            for(int i = 0; i < length; i++){
                if(ocean.isOccupied(row+i, column-1) || ocean.isOccupied(row+i, column) || ocean.isOccupied(row+i, column+1)){
                    return false;
                }
            }
            if(ocean.isOccupied(row+length, column)){
                return false;
            }
        }
        return true;
    }

    @Override
    public void placeShipAt(int row, int column, boolean horizontal, Ocean ocean) {
        hitCount=  0;
        setBowColumn(column);
        setBowRow(row);
        setHorizontal(horizontal);
        if(horizontal){
            for(int i = 0; i < length;i++){
                ocean.setShipArray(row, column+i, this);
            }
        }
        else{
            for(int i = 0; i < length;i++){
                ocean.setShipArray(row+i, column, this);
            }
        }
    }

    @Override
    public boolean shootAt(int row, int column) {
        hitCount++;
        return true;
    }

    @Override
    public boolean isSunk() {
        if(hitCount == length)
            return true;
        else
            return false;
    }
    
    //getters
    public abstract int getLength();
    public int getBowColumn(){
        return bowColumn;
    }
    public int getBowRow(){
        return bowRow;
    }
    public boolean isHorizontal(){
        return horizontal;
    }
    
    //setters
    public void setBowRow(int row){
        bowRow = row;
    }
    public void setBowColumn(int column){
        bowColumn = column;
    }
    public void setHorizontal(boolean horizontal){
        this.horizontal = horizontal;
    }
    public void setLength(int value){
        length = value;
    }
    
//    @Override
//    public String toString(){
//        if(isSunk()){
//            return "x";
//        }
//        return "S";
//    }
}
