package battleship;

public interface ShipAction {

    public String getShipType();

    public boolean okToPlaceShipAt(int row, int column, boolean horizontal, Ocean ocean);

    public void placeShipAt(int row, int column, boolean horizontal, Ocean ocean);

    public boolean shootAt(int row, int column);
    
    public boolean isSunk();
}
