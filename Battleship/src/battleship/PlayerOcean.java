/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import javax.swing.JOptionPane;

/**
 *
 * @author Chase
 */
public class PlayerOcean extends Ocean{
    private int currentShip;

    public PlayerOcean() {
        super();
        currentShip = 0;
    }

    public boolean placeShip(int row, int column, boolean hori) {
        if(armada[currentShip].okToPlaceShipAt(row, column, hori, this)){
            armada[currentShip].placeShipAt(row, column, hori, this);
            currentShip++;
            return true;
        }
        else{
            //JOptionPane.showMessageDialog(null, "alert", "alert", JOptionPane.ERROR);
            System.out.println("wrong spot");
        }
        return false;
    }
    
    public boolean allShipsPlaced(){
        return currentShip == 10;
    }
    
    public int getCurrentShipLength(){
        switch(currentShip){
            case 0:
                return 4;
            case 1:
                return 4;
            case 2:
                return 3;
            case 3:
                return 3;
            case 4:
                return 2;
            case 5:
                return 2;
            case 6:
                return 2;
            default:
                return 1;
        }
    }
}