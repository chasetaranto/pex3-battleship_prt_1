/*
Eliezer Pla && Chaseon Taranto
*/
package battleship;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class AttackButton extends JButton implements ActionListener{
    final int row, column;
    Ocean ocean;
    private Ship ship;
    public AttackButton(int row, int column, Ocean ocean, boolean cheat){
        super();
        this.row = row;
        this.column = column;
        this.ocean = ocean;
        Ship[][] tempArray = ocean.getShipArray();
        ship = tempArray[row][column];
        String pic;
        if(cheat){
            if(ocean.isOccupied(row, column)){
                pic = "Hit.png";
            }
            else{
                pic = "Space.png";
            }
        }
        else{
            pic = "Space.png";
        }
         try {
            Image img = ImageIO.read(getClass().getResource(pic));
            setIcon(new ImageIcon(img));
          } catch (IOException ex) {}
          addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        //System.out.println(row+" , "+column);
        //System.out.println(ocean.isOccupied(row, column));
        shotTaken();
        if(ocean.shootAt(row, column)){
            ship.shootAt(row, column);
            try {
                Image img = ImageIO.read(getClass().getResource("Hit.png"));
                setIcon(new ImageIcon(img));
                shipHit();
              } catch (IOException ex) {}
            removeActionListener(this);
            if(ship.isSunk()){
                //System.out.println("Sunk a "+ship.getShipType());
                JOptionPane.showMessageDialog(null,"Sunk a "+ship.getShipType()+"!");
                if(ocean.isGameOver()){
                    JOptionPane.showMessageDialog(null,"You Won.    Score: "+ocean.getShotsFired());
                }
            }
        }
        else{
            //setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.red));
            setEnabled(false);
            //removeActionListener(this);
        }
    }
    private void shotTaken(){
        AudioInputStream audioIn;
        try {
            audioIn = AudioSystem.getAudioInputStream(BackgroundMusic.class.getResource("Laser_Shoot.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {}
    }
    private void shipHit(){
        AudioInputStream audioIn;
        try {
            audioIn = AudioSystem.getAudioInputStream(BackgroundMusic.class.getResource("Explosion.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {}
    }
    
    public void disableButton(){
        removeActionListener(this);
    }
    
    public void enableButton(){
        addActionListener(this);
    }
}
