/*
Eliezer Pla && Chaseon Taranto
*/
package battleship;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
    public GamePanel(){
        super();
        boolean cheat = false;//Cheat mode control
        Ocean ocean = new Ocean();
        ocean.placeAllShipsRandomly();
        setLayout(new GridLayout(11,11));
        for(int i = 0; i < 11; i++){
            for(int j = 0; j< 11; j++){
                if(i==10 && j<10){
                    add(new JLabel("        "+j));
                }
                else if(j==10 && i<10){
                    add(new JLabel("        "+i));
                }
                else if(i <10 && j< 10)
                    add(new AttackButton(i, j, ocean, cheat));
            }
        }
    }
}
