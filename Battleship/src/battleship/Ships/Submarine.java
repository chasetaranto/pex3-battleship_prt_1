/*
Eliezer Pla && Chaseon Taranto
*/
package battleship.Ships;

import battleship.Ship;

public class Submarine extends Ship{
    private final int length = 1;
    private final String name = "submarine";
    
    public Submarine(){
        super.setLength(length);
    }

    @Override
    public String getShipType() {
        return name;
    }

    @Override
    public int getLength() {
        return length;
    }
    
        @Override
    public String toString(){
        return name.charAt(0)+"";
    }
    
}
