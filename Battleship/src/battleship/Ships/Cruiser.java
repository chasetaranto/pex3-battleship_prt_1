/*
Eliezer Pla && Chaseon Taranto
*/
package battleship.Ships;

import battleship.Ship;

public class Cruiser extends Ship{
    private final int length = 3;
    private final String name = "cruiser";
    
    public Cruiser(){
        super.setLength(length);
    }

    @Override
    public String getShipType() {
        return name;
    }

    @Override
    public int getLength() {
        return length;
    }
    
        @Override
    public String toString(){
        return name.charAt(0)+"";
    }
    
}
