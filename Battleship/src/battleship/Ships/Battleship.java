/*
Eliezer Pla && Chaseon Taranto
*/
package battleship.Ships;

import battleship.Ship;

public class Battleship  extends Ship{
    private final int length = 4;
    private final String name = "battleship";
    
    public Battleship(){
        super.setLength(length);
    }

    @Override
    public String getShipType() {
        return name;
    }

    @Override
    public int getLength() {
        return length;
    }
    
    @Override
    public String toString(){
        return name.charAt(0)+"";
    }

}
