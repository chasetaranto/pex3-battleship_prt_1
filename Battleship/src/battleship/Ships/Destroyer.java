/*
Eliezer Pla && Chaseon Taranto
*/
package battleship.Ships;

import battleship.Ship;


public class Destroyer extends Ship{
    private final int length = 2;
    private final String name = "destroyer";
    
    public Destroyer(){
        super.setLength(length);
    }

    @Override
    public String getShipType() {
        return name;
    }

    @Override
    public int getLength() {
        return length;
    }
    
        @Override
    public String toString(){
        return name.charAt(0)+"";
    }
    
}
