/*
Eliezer Pla && Chaseon Taranto
*/
package battleship.Ships;

import battleship.Ship;

public class EmptySea extends Ship{
    public final int length = 1;
    public final String name = "empty sea";
    public EmptySea(){
        super.setLength(length);
    }
    @Override
    public boolean shootAt(int row, int column){
        return false;
    }
    @Override
    public boolean isSunk(){
        return false;
    }

    @Override
    public String getShipType() {
        return name;
    }

    @Override
    public int getLength() {
        return length; 
    }
    
    @Override
    public String toString(){
        return ".";
    }
}
