package battleship;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PlayerShipPlacePanel extends JPanel{
    
    private PlayerOcean ocean;
    
    public PlayerShipPlacePanel(PlayerAttackPanel gamePanel){
        super();
        boolean cheat = false;//Cheat mode control
        ocean = new PlayerOcean();
        setLayout(new GridLayout(11,11));
        int placeNum = 0;
        for(int i = 0; i < 11; i++){
            for(int j = 0; j< 11; j++){
                if(i==10 && j<10){
                    add(new JLabel("        "+j));
                    placeNum++;
                }
                else if(j==10 && i<10){
                    add(new JLabel("        "+i));
                    placeNum++;
                }
                else if(i <10 && j< 10){
                    System.out.println(placeNum);
                    add(new PlaceButton(i, j, ocean, cheat, gamePanel, this,placeNum++));
                }
            }
        }
    }
    
    public boolean allShpisPlaced(){
        return ocean.allShipsPlaced();
    }
    
    public int getCurrentLength(){
        return ocean.getCurrentShipLength();
    }
}
