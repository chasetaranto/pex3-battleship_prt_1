/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Chase
 */
public class PlaceButton extends AttackButton{
    PlayerOcean ocean;
    PlayerAttackPanel gamePanel;
    PlayerShipPlacePanel placePanel;
    private int numPlaced;
    public PlaceButton(int row, int column, PlayerOcean ocean, boolean cheat, PlayerAttackPanel gamePanel, PlayerShipPlacePanel placePanel, int numPlaced) {
        super(row, column, ocean, cheat);
        this.ocean = ocean;
        this.placePanel = placePanel;
        this.gamePanel = gamePanel;
        this.numPlaced = numPlaced;
        setEnabled(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        System.out.println(numPlaced);
        if(!ocean.allShipsPlaced()){
            int reply = JOptionPane.showConfirmDialog(null, "Horizonal?", "Hor Choose", JOptionPane.YES_NO_OPTION);
            boolean hori = false;
            if(reply == 0){
                hori = true;
            }
            if(ocean.placeShip(row, column, hori)){
                if(hori){
                    for(int i = 0; i < ocean.getCurrentShipLength();i++){
                        (placePanel.getComponent(numPlaced+i)).setEnabled(false);
                    }
                }
                else{
                    for(int i = 0; i < ocean.getCurrentShipLength();i++){
                        (placePanel.getComponent(numPlaced+11*i)).setEnabled(false);
                    }
                }
            }
        }
//        if(ocean.allShipsPlaced()){
//            gamePanel.toggleEnableButtons(true);
//        }
    }
}
